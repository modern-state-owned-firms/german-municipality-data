Research project

# 'Modern state-owned firms' performance: An empirical analysis of productivity, market power, and innovation'
financed by the German Science Foundation (DFG) | 2019-2022

Authors of the following codes: Julia Rechlitz (@jrechlitz), Greta Sundermann (@greta.sundermann), Nicole Wägner (@nwaegner)

All codes here provided are open source. All codes are licenced under the [MIT License](https://spdx.org/licenses/MIT.html) expanded by the [Commons Clause](https://commonsclause.com/). All data sets are licenced under the [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/). 

## Data preparation: EXTERNAL DATA Part II - German Municipality Data

In this step, we prepare external data on German municipalities from the Statistical Offices of the Federation and the Federal States (e.g., Statistik lokal, Regionaldatenbank) and from other sources (e.g., German Environment Agency (UBA), CORINE landuse database). In a later step, this data is merged with the AFiD data. 

**First block: Data from Statistik lokal and Regionaldatenbank**

------------------------------------------------

Municipality-level data is supplied by the the Statistical Offices of the Federation and the Federal States (destatis) in two major sources covering different time periods: 
- [Statistik lokal](https://webshop.it.nrw.de/details.php?id=14349) (2003-2011) and 
- [Regionaldatenbank](https://www.regionalstatistik.de/genesis/online/) (GENESIS) (2008-2018).
These sources gather various data on population size, area, election outcomes, (agricultural) land use, and water tariffs.

<details><summary>Click here to see a full list of the variables that we use.</summary>

| variable | description | source | available years |
| --------- | ---------- | -------- | -------- |
| AGS | Municipality code (Amtlicher Gemeindeschlüssel) |destatis | 2001-2018 |
| Gemeinde | Municipality name | destatis | 2001-2018 |
| Jahr | Year of observation | destatis | 2001-2018 |
| Bevölkerung..insgesamt. | Total population |    destatis | 2001-2018 |
| Bevölkerung.unter.18.Jahre| Population aged < 18 years | destatis | 2001-2018 |
| Bevölkerung.18.60.Jahre | Population aged 18 - 60 years | destatis | 2001-2018 |
| Bevölkerung.über.60.Jahre| Population aged > 60 years | destatis | 2001-2018 |
| Flaeche_qkm | Area in km2 |destatis | 2001-2018 |
| Art.Wahl| Type of election |destatis | 2001-2015 |
| Bundesland | Federal state name |destatis | 2001-2018 |
| Wahlberechtigte | Number of elegible voters |destatis | 2001-2015 |
| Wahlbeteiligung | Voter turnout |destatis | 2001-2015 |
| Gueltige.Stimmen| Number of valid votes | destatis | 2001-2015 |
| CDU.CSU | Votes for CDU/CSU | destatis | 2001-2015 |
| SPD | Votes for SPD | destatis | 2001-2015 |                 
| Gruene| Votes for BUENDNIS90/Grüne | destatis | 2001-2015 |
| FDP| Votes for FDP | destatis | 2001-2015 |
| Die.Linke| Votes for Die Linke | destatis | 2001-2015 |
| AFD | Votes for AFD | destatis | 2001-2015 |
| Sonstige.Parteien|  Votes for other parties | destatis | 2001-2015 |
| LF_ha | Agricultural land in ha | destatis | 2001-2007 |
| Acker_ha | Arable land in ha |destatis | 2001-2007 |
| Siedl_ha | Settlement area in ha| destatis | 2008-2015|       
| LW_ha | Agricultural land in ha |destatis | 2008-2015|  
| Wald_ha | Forest area in ha |destatis | 2008-2015|  
| Wass_ha | Water area in ha | destatis | 2008-2015|  
| rinder_total_n |Total number of cattle at the district level | destatis | 2010-2016 |
| lwb_with_100gv | Number of farms with at least 100 livestock units| destatis | 2010, 2016|
| lwb_with_livestock | Number of farms with livestock | destatis | 2010, 2016 |
| lwb_cattle | Number of farms with cattle | destatis | 2010, 2016 |
| lwb_cows | Number of farms with cows| destatis | 2010, 2016 |
| lwb_pigs | Number of farms with pigs | destatis | 2010, 2016 |
| lwb_breeding_pigs | Number of farms with breeding pigs| destatis | 2010, 2016 |
| lwb_sheeps | Number of farms with sheeps | destatis | 2010, 2016 |
| lwb_nb_cattle | Number of cattle| destatis | 2010, 2016 |
| lwb_nb_cows |Number of cows | destatis | 2010, 2016 |
| lwb_nb_pigs | Number of pigs | destatis | 2010, 2016 |
| lwb_nb_breeding_pigs | Number of breeding pigs | destatis | 2010, 2016 |
| lwb_nb_sheeps | Number of sheeps| destatis | 2010, 2016 |
| organic_lw_betriebe_n | Number of organic farms| destatis | 2001, 2003, 2005, 2007, 2010, 2016 |
| organic_lf_ha | Area of organic farm land| destatis | 2001, 2003, 2005, 2007, 2010, 2016 |
| lw_betriebe_n_kr | Number of farms within district| destatis | 2010, 2016 |
| betriebe_acker_n_kr | Number of farms with farmland within district | destatis | 2010, 2016 |
| betriebe_dk_n_kr | Number of farms with permanent cultures within district| destatis | 2010, 2016 |
| betriebe_gruenland_n_kr | Number of farms with greenland within district| destatis | 2010, 2016 |
| acker_ha_kr | Area of farmland within district | destatis | 2010, 2016 |
| uaa_ha_kr | Utilized agricultural area within district| destatis | 2010, 2016 |
| gruenland_ha_kr | Area of greenland within district| destatis | 2010, 2016 |
| lws_b_total | Number of agricultural holdings| destatis | 2010, 2016 |
| lws_b_u_5ha | Number of agricultural holdings with less than 5 ha farm area| destatis | 2010, 2016 |
| lws_b_5_10_ha | Number of agricultural holdings with between 5 and 10 ha farm area| destatis | 2010, 2016 |
| lws_b_10_20_ha | Number of agricultural holdings with between 10 and 20 ha farm area| destatis | 2010, 2016 |
| lws_b_20_50_ha| Number of agricultural holdings with between 20 and 50 ha farm area| destatis | 2010, 2016 |
| lws_b_50_100_ha| Number of agricultural holdings with between 50 and 100 ha farm area| destatis | 2010, 2016 |
| lws_b_100_200_ha| Number of agricultural holdings with between 100 and 200 ha farm area| destatis | 2010, 2016 |
| lws_b_200plus| Number of agricultural holdings with more than 200 ha farm area| destatis | 2010, 2016 |
| verbrauchsabh.Entgelt.in.EUR.m3 | variable fee water tariff | destatis | 2008-2016 |
| verbrauchsunabh.Entgelt.in.EUR | fixed fee water tariff | destatis | 2008-2016 |


</details>



**Second block: Data from UBA and other sources**

------------------------------------------------

This part merges the first block (described above) with municipality-level data from other sources:
- [Nitrate readings](https://gis.uba.de/maps/resources/apps/nitratbericht_eu_richtlinie/index.html?lang=de) from German Environment Agency (Umweltbundesamt, UBA)
- [Groundwater bodies and river basins](https://geoportal.bafg.de/mapapps/resources/apps/WKSB/index.html?lang=de) from The German Federal Institute of Hydrology (BfG)
- [CORINE landcover (CLC) database](https://land.copernicus.eu/pan-european/corine-land-cover)
- [Administrative shapefiles (VG250)](https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-250-000-ebenen-stand-01-01-vg250-ebenen-01-01.html) from the Federal Agency from Cartography and Geodesy
- [Weather monitor readings](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/historical/) from Deutscher Wetterdienst (DWD)
- Nitrogen area balances from  Institut für Landschaftsökologie und Ressourcenmanagement, Justus Liebig University Giessen, contact: Dr. Martin Bach and Uwe Häußermann (Bach et al.)
- [Settlement structure](https://www.bbsr.bund.de/BBSR/DE/forschung/raumbeobachtung/Raumabgrenzungen/raumabgrenzungen-uebersicht.html) from the Federal Institute for Research on Building, Urban Affairs, and Spatial Development


The second data frame is available for the years 2008-2016 and based on administrative shapefiles; data from other sources is merged via geospatial operations, e.g., nitrate and weather readings from different monitor locations are interpolated at the geographical center of municipalities.

<details><summary>Click here to see a full list of the variables that we use.</summary>

| variable | description | source | 
| --------- | ---------- | -------- | 
| RS | Municipality code (Regionalschlüssel, 2019) | Shapefiles VG250 |
| Jahr | Year of observation | Shapefiles VG250 |
| rs_timevar | Municipality code (time-varying) | Shapefiles VG250 |
| name_timevar | Municipality name (timevarying) | Shapefiles VG250 |
| mun_type | Type of municipality | Shapefiles VG250 |
| county_code | County code (5-digit AGS) | Shapefiles VG250 |
| county_name | County name | Shapefiles VG250 |
| county_type | County type | Shapefiles VG250 |
| county_area | County area | Shapefiles VG250 |
| closest_station_id | ID of closest nitrate monitor | UBA |
| closest_station_distance | Distance to closest nitrate monitor in m | UBA |
| closest_station_nitrate | Nitrate reading at closest nitrate monitor in mg/l | UBA |
| closest_station_in_mun | Dummy equal to one if closest nitrate monitor located within municipality's borders | UBA |
| closest_station_feg | ID of closest nitrate monitor within municipality's river basin (Flusseinzugsgebiet, FEG) | UBA, BfG |
| closest_station_gwk | ID of closest nitrate monitor within municipality's groundwaterbody (Grundwasserkörper, GWK) | UBA, BfG |
| closest_station_in_feg | Dummy equal to one if closest nitrate monitor located within municipality's river basin (Flusseinzugsgebiet, FEG) | UBA, BfG |
| closest_station_in_gwk | Dummy equal to one if closest nitrate monitor located within municipality's municipality's groundwaterbody (Grundwasserkörper, GWK) | UBA, BfG |
| nitrate_1km | Inverse distance weighted nitrate reading of monitors within 1 km distance | UBA |
| ... | ... | UBA |
| nitrate_200km | Inverse distance weighted nitrate reading of monitors within 200 km distance | UBA |
| feg | Name of municipality's river basin | UBA |
| gwk | Name of municipality's groundwater body | BfG | 
| feg_nitrate_1km | Inverse distance weighted nitrate reading of monitors within 1 km distance in the same river basin | UBA, BfG |
| ... | ... | UBA, BfG |
| feg_nitrate_200km | Inverse distance weighted nitrate reading of monitors within 200 km distance in the same river basin | UBA, BfG |
| gwk_nitrate_1km | Inverse distance weighted nitrate reading of monitors within 1 km distance in the same groundwater body | UBA, BfG |
| ... | ... | UBA, BfG |
| gwk_nitrate_200km | Inverse distance weighted nitrate reading of monitors within 200 km distance in the same groundwater body | UBA, BfG |
| share_city | Share of urban area in municipality | CLC |
| share_share_industrycity | Share of industry area in municipality | CLC |
| share_mining_landfills | Share of mining and landfill area in municipality | CLC |
| share_city_greenarea | Share of green urban area in municipality | CLC |
| share_farmland | Share of farmland area in municipality | CLC |
| share_wine | Share of wine growing area in municipality | CLC |
| share_fruit | Share of fruit growing area in municipality | CLC |
| share_greenland | Share of greenland area in municipality | CLC |
| share_forest | Share of forest area in municipality | CLC |
| share_bushes | Share of bush area in municipality | CLC |
| share_area_without_vegetation | Share of vegetation free area in municipality | CLC |
| share_marshlands | Share of marshland area in municipality | CLC |
| share_water | Share of water area in municipality | CLC |
| closest_station_share_city | Share of urban area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_share_industrycity | Share of industry area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_mining_landfills | Share of mining and landfill area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_city_greenarea | Share of green urban area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_farmland | Share of farmland area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_wine | Share of wine growing area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_fruit | Share of fruit growing area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_greenland | Share of greenland area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_forest | Share of forest area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_bushes | Share of bush area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_area_without_vegetation | Share of vegetation free area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_marshlands | Share of marshland area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| closest_station_share_water | Share of water area in 300 m radius around closest nitrate monitor  | UBA, CLC |
| idw100_mun_wind_speed_max | Maximum annual wind speed (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_wind_speed | Average annual wind speed (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_vapor_pressure | Average annual vapor pressure (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_atm_pressure | Average annual atmospheric pressure (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_temperature | Average annual temperature in °C (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_humidity | Average annual relative humidity in % (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_temperature_max | Maximum annual temperature in °C (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_temperature_min | Minimum annual temperature in °C (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_avg_precipitation | Average annual precipitation in mm/day (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_sum_precipitation | Sum of annual precipitation in mm (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_avg_sunshine | Average annual sunshine hours in h/day (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_sum_sunshine | Sum of annual sunshine hours in h (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_avg_snow | Average annual snowfall in cm (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| idw100_mun_sum_snow | Sum of annual snowfall in cm (inverse distance weighted average of readings at weather monitors within 100 km distance to municipality's center) | DWD |
| supply_n |Nitrogen input, total in kg N/ha UAA | Bach et al. |
| supply_removal_n | Nitrogen removal, total in kg N/ha UAA| Bach et al. |
| supply_mineral_n | Mineral fertilizer input, total in kg N/ha UAA| Bach et al. |
| uaa | Utilized agricultural land| Bach et al. |
| closest_station_rs| | |
| Siedlungstyp_grob_alt | settlement structure as of 2015 | BBSR, 2015 |
| Siedlungstyp_grob_neu | settlement structure as of 2020 | BBSR, 2020 |
| Siedlungstyp_fein_neu | detailed settlement structure as of 2020| BBSR, 2020 |
| hoehe_m | normal height (above mean sea level) in m | GeoBasis-DE |



</details>

