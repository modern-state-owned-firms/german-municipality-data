#==============================================================================
# Merge municipality-level data set with environmental data, including...
#   - Nitrate readings
#   - Land use and agriculture (fertilizer use, animal counts, ...)
#   - Weather readings
#
# Authors: Julia Rechlitz, Nicole Waegner, Greta Sundermann
#
# Input data sets:
#   - Nitratdatensatz mit Umweltvariablen (2006 - 2018)
#
# Output data set: 4193_Gemeindedatensatz_2001_2018.rds
#==============================================================================

#==============================================================================
# 1. load packages
#==============================================================================

library(tidyverse)
# lintr::lint(paste0(syntaxpfad, "/02_merge_data_nitrate.R"))

#==============================================================================
# 2. data preparation
#==============================================================================

# Prepare mun-level data set
# ---------------------------
# Data frame with mun-level data from Statistik lokal and regionalstatistik.de
# obtained from script 01
head(m.settc) # Adjust df name once script 01 is finalized
matches <- data.frame(m.settc)

# NO3 and weather measurements are available from 2003 - 2018
matches_no3 <- filter(matches, Jahr >= 2003)
rm(m.settc)

# load nitrate (NO3) data set
# ---------------------------
no3 <- read_rds(paste0(datenpfad, "/mun_nitrate_ags.rds"))
addmargins(table(no3$year))
names(no3)

# Rename mun-level weather variables
no3 <- no3 %>%
  rename_at(vars(wind_speed_max:sum_snow),
            function(x) {
              paste0("idw100_mun_", x)
              }) %>%
  rename_at(vars(closest_station_wind_speed_max:closest_station_sum_snow),
            function(x) {
              paste0("idw100_", x)
              })

# Check for duplicates in NO3 data
# --------------------------------
# Duplicates are ok, they exist due to Teilausgliederungen and Aufloesungen
# of municipalities
temp <- no3 %>% group_by(id_mun, year) %>% mutate(count = n())
dim(temp)
table(temp$count, useNA = "ifany")
names(no3)

#==============================================================================
# 3. merge the data sets
#==============================================================================

# Merge via AGS
# --------------------------------
# NO3 mun-level data is based on shapefiles that use the AGS from 2018/2019.
# We merge via a time-varying RS/AGS created from annual Gemeindeverzeichnis
# and the list of annual changes in GV.
# We do NOT merge via RS due to errors in Verbandsgem-schluessel in
# original xlsx files provided by destatis.
matches_ags <- left_join(select(matches_no3, Jahr, AGS_n2, Gemeinde),
                         no3,
                         by = c("AGS_n2", "Jahr" = "year"))
head(matches_ags)
sapply(matches_ags, function(x) length(which(is.na(x))))

# Remove duplicates after merge
# --------------------------------
# Duplicates are created due to Namensaenderungen, Teilausgliederungen and
# Aufloesungen
matches_ags <- matches_ags %>% group_by(AGS_n2, Jahr) %>% mutate(dupl = n())
table(matches_ags$dupl)

# Remove duplicates based on Gemeinde name
dupl <- filter(matches_ags, dupl > 1)
dupl[, c("Gemeinde", "name_timevar", "name_2019")]
dupl <- dupl %>%
  mutate(Flag = c("No", "Yes")[1 + str_detect(Gemeinde,
                                              as.character(name_2019))])
table(dupl$Flag)
addmargins(table(dupl$Jahr, dupl$Flag))
addmargins(table(dupl$Jahr))
dupl <- filter(dupl, Flag == "Yes")
# here No will be dropped

# Remove duplicated due to Namensaenderungen
length(which(duplicated(dupl[, 1:182])))
dupl <- dupl[!duplicated(dupl[, 1:182]), ]

# Remaining duplicates are due to errors in Verbandsgem.-schluessel
length(which(duplicated(dupl[, c("Jahr", "AGS_n2")])))
dupl <- dupl[!duplicated(dupl[, c("Jahr", "AGS_n2")]), ]

dupl <- dupl %>% group_by(AGS_n2, Jahr) %>% mutate(dupl = n())
table(dupl$dupl)

# Create df without duplicated AGS-Jahr combinations
nrow(matches_ags)
length(unique(matches_ags$AGS_n2))

matches_ags_unique <- bind_rows(
  filter(matches_ags, dupl == 1),
  dupl)

nrow(matches_ags_unique)
length(unique(matches_ags_unique$AGS_n2))

# Check: Which municipalities were removed unduly?
# -----------------------------------------------
# Several municipalities are lost due to Aufloesung of one mun into two muns
# (given that the Aufloesung happens before 2018/2019).
# Average nitrate readings do not differ too much for these muns (as they're
# geographical neighbors) so we arbitrarily select one of the two muns.
# same logic should apply to average weather conditions.
lost_ags <- unique(matches_ags$AGS_n2)[which(
  !(unique(matches_ags$AGS_n2) %in% unique(matches_ags_unique$AGS_n2)))]
lost_ags

temp <- filter(matches_ags, AGS_n2 %in% lost_ags)
temp <- temp[!duplicated(temp[, c("Jahr", "AGS_n2")]), ]

matches_ags_unique <- rbind(matches_ags_unique, temp)
length(unique(matches_ags_unique$AGS_n2))

# Check: Only Laender, Verbands-gem, and erroneous obs not merged?
# ----------------------------------------------------------------
# Mostly yes, but single mun-year combinations cannot be merged due to invalid
# AGS_n2 in statistik lokal / regionalstatistik data (new AGS used "too early"),
# e.g. Trittenheim in 2011, Schladenin 2013
test <- filter(matches_ags_unique, is.na(rs_timevar))
table(test$Gemeinde)

rm(temp, test, matches_no3)

# Final merge with all variables included
# ---------------------------------------
nrow(matches)
matches <- left_join(matches,
                     select(matches_ags_unique, -Gemeinde),
                     by = c("AGS_n2", "Jahr"))
nrow(matches)

# add deepth of clostest_station_id to data frame
# ------------------------------------------------
stations <- readRDS("./data/nitrate_corine_land_use_2008-2012_extension_5.rds")
str(stations)

stations = as.data.frame(stations) %>% 
  select(id, deepth_le, deepth_ue) %>%
  filter(!is.na(deepth_ue) | !is.na(deepth_le)) %>% distinct

matches <- left_join(matches, stations, by = c("closest_station_id" = "id"))

#==============================================================================
# 4. Create and save final data frame
#==============================================================================

# Drop variables that are not part of the data use request
# --------------------------------------------------------
matches <- matches %>%
  # drop variables that are "duplicated" due to merges
  select(-district, -mun_ags, -mun_name, -mun_area) %>%
  # drop auxiliary variables
  select(-dupl, -Flag) %>%
  # drop soil characteristics
  select(-starts_with("soil_"), -starts_with("closest_station_soil_")) %>%
  # drop groundwater availability characteristics
  select(-c(gwtype_important:closest_station_permeability)) %>%
  # drop geo-coordinates of municipality center
  select(-c(mun_lat, mun_lon))
names(matches)

# Rename and reorder variables
# ----------------------------
matches <- matches %>%
  rename(rs_2019 = id_mun) %>%
  select(Jahr, AGS:AGS_n2, Gemeinde,
         mun_type, starts_with("county_"),
         everything(),
         rs_2019, name_2019, rs_timevar, name_timevar)
names(matches)

str(matches)

# check data for missing values
# -------------------------------
addmargins(table(is.na(matches$AGS)))
addmargins(table(is.na(matches$AGS_char)))
addmargins(table(is.na(matches$AGS_n2)))
addmargins(table(is.na(matches$Jahr)))
addmargins(table(is.na(matches$Gemeinde)))
addmargins(table(is.na(matches$Bundesland)))

# check length of identifiers
# ---------------------------
addmargins(table(nchar(matches$AGS)))
addmargins(table(nchar(matches$AGS_char)))
addmargins(table(nchar(matches$AGS_n2)))

# complete Bundesland-variable
# -------------------------------
matches <- matches %>% mutate(AGS_8dig = ifelse(nchar(AGS_n2) == 7, 
                                                paste0("0", AGS_n2), AGS_n2),
                              Bundesland = substring(AGS_8dig, 1, 2),
                              Bundesland = ifelse(Bundesland >= 71, "07", Bundesland),
                              Bundesland = as.numeric(Bundesland),
                              Bundesland = factor(Bundesland, labels = c("Schleswig-Holzstein",
                                                   "Hamburg",
                                                   "Niedersachsen",
                                                   "Bremen",
                                                   "Nordrhein-Westfalen",
                                                   "Hessen",
                                                   "Rheinland-Pfalz",
                                                   "Baden-Wuerttemberg",
                                                   "Bayern",
                                                   "Saarland",
                                                   "Berlin",
                                                   "Brandenburg",
                                                   "Mecklenburg-Vorpommern",
                                                   "Sachsen",
                                                   "Sachsen-Anhalt",
                                                   "Thueringen")))
levels(matches$Bundesland)
addmargins(table(matches$Bundesland))

# structure data frame 
names(matches)
matches <- matches %>% ungroup() %>% 
                       select(Jahr, AGS, AGS_n2, AGS_char, AGS_8dig,
                              Gemeinde, Bundesland, 
                              Bevoelkerung..insgesamt.:Bevoelkerung.ueber.60.Jahre,
                              hoehe_m,
                              Flaeche_qkm,
                              Siedlungstyp_grob_alt:Siedlungstyp_fein_neu,
                              Wahlbeteiligung, Wahlberechtigte, AFD:CDU.CSU, Sonstige.Parteien, Gueltige.Stimmen,Art.Wahl,
                              verbrauchsabh.Entgelt.in.EUR.m3, verbrauchsunabh.Entgelt.in.EUR,
                              LF_ha, Acker_ha, Wass_ha, Wald_ha, Siedl_ha, LW_ha,
                              mun_type:county_area,rs_2019:nuts_code, deepth_ue, deepth_le,
                              closest_station_id:rs_timevar, everything()) %>% select(-c(match))
names(matches)

# are there any duplicates in the final data frame? 
# ------------------------------------------------
# check by AGS, AGS_n2, AGS8dig
matches %>% group_by(AGS, Jahr) %>% mutate(dupl = n()) %>% filter(dupl > 1)
matches %>% group_by(AGS_n2, Jahr) %>% mutate(dupl = n()) %>% filter(dupl > 1)
matches %>% group_by(AGS_char, Jahr) %>% mutate(dupl = n()) %>% filter(dupl > 1)
matches %>% group_by(AGS_8dig, Jahr) %>% mutate(dupl = n()) %>% filter(dupl > 1)
# --> no!

# how many observations are there per year
# ---------------------------------------
addmargins(table(matches$Jahr))

# how many observations are there per variable
# --------------------------------------------
complete <- matches %>% group_by(Jahr) %>%
                        summarize_at(vars(1:184), list(~sum(!is.na(.), na.rm = T)))

# random checks 
# ------------
# compare some nitrate readings per station id with raw data 
# "2019-09-02_UBA_Nitrat_2012-2016_Hiliges_per_mail.csv"
#View(filter(matches, closest_station_id == "32417780", Jahr == 2012)) # correct
#View(filter(matches, closest_station_id == "25350008", Jahr == 2014)) # correct
#View(filter(matches, closest_station_id == "52456001", Jahr == 2016)) # correct

# compare some precipitation sums with findings online
filter(matches, Gemeinde == "Bad Bergzabern", Jahr == 2006) %>% select(Jahr:AGS_8dig, Gemeinde, Bundesland, idw100_mun_sum_precipitation) # check
filter(matches, Gemeinde == "Cottbus, Kreisfreie Stadt", Jahr == 2012) %>% select(Jahr:AGS_8dig, Gemeinde, Bundesland, idw100_mun_sum_precipitation) # check
filter(matches, Gemeinde == "Mannheim, Stadtkreis", Jahr == 2005) %>% select(Jahr:AGS_8dig, Gemeinde, Bundesland, idw100_mun_sum_precipitation) # check

# compare district data on nitrogen balances with 
# "DatenBJ2017_SundermannDIW_010420.xls"
sample_n(select(matches, Jahr, county_name, county_code, supply_n:uaa), 5) # correct

names(matches)

rm(dupl, matches_ags, matches_ags_unique, no3, stations, complete)
