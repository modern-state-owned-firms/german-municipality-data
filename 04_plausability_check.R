#==============================================================================
# Check correctness of data frame
#
# Authors: Greta Sundermann
#
# Input data sets:
#   - 02_merge_data_nitrate.R
#==============================================================================

#==============================================================================
# 1. load packages
#==============================================================================

library(dplyr)

#==============================================================================
# 2. data preparation
#==============================================================================

# load data
#data <- readRDS(paste0(outputpfad, "/4193_Gemeindedatensatz_2001_2018.rds"))
data <- readRDS(paste0(outputpfad, "/4193_Gemeindedatensatz_2001_2018_pollution_extension.rds"))

stoffliste <- read_delim("data/opengepdata-nrw-stoffliste.csv", 
                         delim = ";", escape_double = FALSE, trim_ws = TRUE) 
stoffliste <- stoffliste %>% select(c(1:3))
names(stoffliste)
filter(stoffliste, name == "Nitrat" | name == "Ammonium" | name == "Ortho-Phosphat")

#==============================================================================
# 3. check for errors
#==============================================================================

dim(data)

#==============================================================================
# 3.1 Variables and variable type
#==============================================================================

names(data)
# data contains information on:
# - Jahr / AGS
# - Bevölkerung
# - Siedlungstyp (grob/fein)
# - Siedlungsflächen
# - Wahlbeteiligung
# - Wasserentgelte
# - Nitratdaten (clostest station, weighted distance by gwk/feg)
# - Corine landcover data
# - Stickstoffflächenbilanzen
# - Landwirtschaftsbetriebe mit Vieh, Viehzahlen
# - Bioanbau
# - Größenklassen Landwirtschaft

# --> Es fehlt die Tiefe der Nitratmessstationen noch ergänzen.

str(data, list.len = 183)

# some observations have the wrong class
# - Bevölkerung insgesamt
# - Fläche_qkm
# - Acker_ha
# - LF_ha

# --> fixed

#==============================================================================
# 3.2 missing observations
#==============================================================================

# summarize number of missing observations per variable and year
missings <- data %>% group_by(Jahr) %>%
                     summarize_at(vars(1:182), list(~sum(is.na(.), na.rm = T)))

# summarize number of complete observations per variable and year
# this might be more useful, as data frame consist also of e.g. district,
# state level which is not recorded by each survey
complete <- data %>% group_by(Jahr) %>%
  summarize_at(vars(1:182), list(~sum(!is.na(.), na.rm = T)))


temp = data %>% select(Jahr:Siedlungstyp_fein_neu) %>%
  filter(is.na(Siedlungstyp_fein_neu))

View(matches)

# warning:
# ------------------------------------------------------------------------------
# - Caros Sieldungstypen are not available for Land/Regierungsbezirk/Landkreis/
#   Verbandsgemeinden/, and for some only the Sieldungstyp_grob_alt available
# --> Caro hat Daten selbst geprüft, alles gut!

# - Starting in 2014 until 2018 there, there are observations on district-level
#   specifically for Berlins districts but only completed for the settlement data and flaeche_qkm
#   for all other variables it is NA 
# --> fixed issue

# - Julia: Wahldaten 2007 und 2015 wenige Beobachtungen
# --> korrekt!

# - lw_nb_breeding_pigs only available for 2010 and 2016, interpolation missing
# - organic farming was available for 2007, 2010, 2013, 2016 --> gab years
#   2008 and 2009 could have been interpolated... missing here, too.
# --> has been fixed!

# - data on deepth_le and deepth_ue for closest distance station was missing
# -> added data

# - nitrate data contains a time-invariant group-id, namely "rs_2019" which is
#   complete for all observations during the period 2008-2016

# - weather data is only available for 2008 - 2016
# -->

names(data)


#==============================================================================
# 3.3 check pollution data
#==============================================================================

#     1244 Nitrat         NO3     
#     1248 Ammonium       NH4     
#     1263 Ortho-Phosphat O-PO4 

pollutants_agri = data %>% select(Jahr:Bundesland, 
                                  starts_with("idw_subs_1244"),
                                  #starts_with("idw_subs_1248"),
                                  #starts_with("idw_subs_1263"),
                                  starts_with("nitrate_"))


temp = pollutants_agri %>% group_by(Jahr) %>%
  summarize_at(vars(idw_subs_1244_1km:nitrate_200km), list(~sum(!is.na(.), na.rm = T)))

test = pollutants_agri %>% group_by(Jahr) %>% summarize_at(vars(idw_subs_1244_1km:nitrate_200km), list(~mean(. , na.rm = T)))

