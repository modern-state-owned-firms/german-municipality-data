#==============================================================================
# Master file: German municipality data 
#
# This master file executes different r codes that merge data on German 
# municipalities from different data sources. All data sets are merged using 
# the "amtliche Gemeindeschluessel" (AGS) and year as identifier. 
#
# Authors: Julia Rechlitz                                                                                    
#                                                                                    
# Calls the following R code:
# "01_merge_data_regionalstat_lokalstat.R
# "02_merge_data_nitrate.R" 
#==============================================================================

#==============================================================================
# 1. Set up R
#==============================================================================

# clean memory 
#--------------
rm(list = ls())

# Tell R to use fixed notation instead of exponential notation
#-------------------------------------------------------------
options(scipen = 999)


#==============================================================================
# 2. Define working environment
#==============================================================================

# The variable FDZ describes, where the code is executed
#	--- 0: Julia's computer
#	--- 1: Greta's computer
# ---	2: Nicole's computer 
#--------------------------------------------------------------------

PC <- 1


#	Working environment 0: Julia's computer
#	---------------------------------------

if (PC == 0)  {
  datenpfad         <- "C:/Users/julia/Google Drive/FDZ_Datenkauf/Gemeindedaten_FDZ/01_excel_dataset_input/single_variables_csv_files"
  syntaxpfad        <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/afid-data/german-municipality-data" 
  outputpfad        <- "C:/Users/julia/Google Drive/FDZ_Datenkauf/Gemeindedaten_FDZ/03_Gemeindedatensatz_ohne_SUF" 

  outputname        <- "log_merge_municipality_data" 
  syntaxname1       <- "01_merge_data_regionalstat_lokalstat"
  syntaxname2       <- "02_merge_data_nitrate"
  syntaxname3       <- "03_merge_data_pollutants"
}


#	Working environment 1: Greta's computer
#	---------------------------------------
# Bitte Pfade anpassen. Daten liegen jetzt erstmal weiterhin im drive ordner
# nur der Code ist umgezogen zu gitlab

if (PC == 1) {
  datenpfad         <- paste0(getwd(), "/data")
  syntaxpfad        <- getwd()
  outputpfad        <- paste0(getwd(), "/output")
  
  outputname        <- "log_merge_municipality_data" 
  syntaxname1       <- "01_merge_data_regionalstat_lokalstat"
  syntaxname2       <- "02_merge_data_nitrate"
  syntaxname3       <- "03_merge_data_pollutants"}


#	Working environment 2: Nicole's computer
#	---------------------------------------

if (PC == 2) {
  datenpfad         <- paste0(getwd(), "/data")
  syntaxpfad        <- getwd()
  outputpfad        <- paste0(getwd(), "/output")
  
  outputname        <- "log_merge_municipality_data" 
  syntaxname1       <- "01_merge_data_regionalstat_lokalstat"
  syntaxname2       <- "02_merge_data_nitrate"
  syntaxname3       <- "03_merge_data_pollutants"
}

#==============================================================================
# 3. Call different R codes
#==============================================================================

# start the log file
#---------------------
sink(paste(outputpfad, "/", outputname, ".log", sep = ""), append = FALSE, type = c("output", "message"), split = TRUE)


# execute R codes
#-----------------
source(paste(syntaxpfad, "/", syntaxname1, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)
source(paste(syntaxpfad, "/", syntaxname2, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)
source(paste(syntaxpfad, "/", syntaxname3, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)

# close the log file
#-------------------
sink()

